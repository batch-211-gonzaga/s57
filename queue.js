let collection = [];
// Write the queue functions below. 


//Gives all the elements of the queue
function print(){
  return collection;
}

//Adds an element/elements to the end of the queue
function enqueue(item){
  //collection.push(item);
  const newCollection = [];

  for (let i = 0; true; i++) {
    if (collection[i])
      newCollection[i] = collection[i];
    else {
      newCollection[i] = item;
      break;
    }
  }

  collection = newCollection;
  return collection;
}

//Removes an element in front of the queue
function dequeue(){
  //collection.shift();
  const newCollection = [];

  for (let i = 1; true; i++) {
    if (collection[i])
      newCollection[i-1] = collection[i];
    else
      break;
  }

  collection = newCollection;
  return collection;
}


//Shows the element at the front
function front(){
  return collection[0];
}

//Shows the total number of elements
function size(){
  let len = 0;

  for (let i = 0; true; i++) {
    if (collection[i])
      len++;
    else
      break;
  }

  return len;
}

//Gives a Boolean value describing whether the queue is empty or not
function isEmpty(){
  if (collection[0])
    return false;
  else
    return true;
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};
